#!/usr/bin/env bash


create_xvfb_unit() {
cat << EOM > /etc/systemd/system/xvfb.service
[Unit]
Description=X Virtual Frame Buffer Service
After=network.target

[Service] 
Type=simple
ExecStart=/usr/bin/Xvfb :10 -screen 0 1920x1080x24 -ac

[Install]
WantedBy=selenium.service
EOM
    echo '/etc/systemd/system/xvfb.service Created'
}

create_selenium_server_unit() {
cat <<EOM > /etc/systemd/system/selenium.service
[Unit]
Description= Selenium Browser Automation
After=xvfb.service

[Service]
PIDFile=/var/run/selenium.pid
User=${SELENIUM_USER}
WorkingDirectory=${SELENIUM_HOME}
Environment=DISPLAY=:10.0
Environment=DBUS_SESSION_BUS_ADDRESS=/dev/null
Environment=LOG_LEVEL=ALL
Environment=LOG_DIR=${LOG_DIR}
Environment=VERBOSE=true
ExecStart=/usr/bin/java -jar /usr/bin/selenium-server-standalone.jar \
        -Dwebdriver.chrome.driver=/usr/lib/chromium-browser/chromedriver \
        -Dselenium.LOGGER.level=\${LOG_LEVEL} \
        -Dselenium.LOGGER=\${LOG_DIR}/selenium.log \
        -Dwebdriver.chrome.logfile=\${LOG_DIR}/chrome_driver.log \
        -Dwebdriver.chrome.verboseLogging=\${VERBOSE}
ExecStop=/bin/kill \$(/bin/cat /var/run/selenium.pid)

[Install]
WantedBy=multi-user.target
EOM
    echo '/etc/systemd/system/selenium.service created' 
}

create_selenium_user() {
    groupadd ${SELENIUM_USER}
    useradd -g ${SELENIUM_USER} -d ${SELENIUM_HOME} -s /bin/sh ${SELENIUM_USER} --create-home
    echo 'User selenium created'
    echo 'Creating log directory'
    mkdir -p ${LOG_DIR}
    chown -R selenium:selenium ${LOG_DIR}
}

if (( $EUID != 0 )); then
    echo 'Run as root'
    exit 1
fi

#Check if xvfb installed. 
dpkg -s xvfb &> /dev/null
if [ $? -eq 0 ]; then
    echo "[Skipped] installing xvfb"
else
    echo 'Package Xvfb is not found, Installing'
    apt-get install -y xvfb
fi

if [ -f /etc/systemd/system/xvfb.service ]; then
    echo '[Skipped] Creating service xvfb'
else
    echo 'Creating xvfb service...'
    create_xvfb_unit
fi

if [ ! -f /usr/bin/java ]; then
    echo "Java not found, Installing ..."
    apt-get install -y openjdk-8-jre
fi

if [ ! -f /usr/lib/chromium-browser/chromedriver ]; then 
    echo 'Chrome driver not found, Installing...'
    apt-get install -y chromium-chromedriver
fi

SELENIUM_USER=selenium
SELENIUM_HOME=/usr/local/lib/selenium
LOG_DIR=/usr/local/lib/selenium/logs
create_selenium_user

if [ ! -f /usr/bin/selenium-server-standalone.jar ]; then
    echo "Downloading Selenium-server-standalone-2.53.1"
    wget -O - "http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar" > /usr/bin/selenium-server-standalone.jar

    chmod +x /usr/bin/selenium-server-standalone.jar
fi

if [ -f /etc/systemd/system/selenium.service ]; then 
    echo '[Skipped] Creating service selenium'
else
    echo 'Creating selenium service...'
    create_selenium_server_unit
fi


echo 'Starging services'

systemctl daemon-reload
systemctl enable xvfb.service 
systemctl enable selenium.service

systemctl start xvfb.service
systemctl start selenium.service

echo 'Install finished...'
